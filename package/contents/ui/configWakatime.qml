import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.0
import QtMultimedia 5.4

ColumnLayout {
    id: appearancePage

    property alias cfg_wakatime_enabled: wakatime_enabled.checked
    property alias cfg_wakatime_api_key: wakatime_api_key.text
    property alias cfg_wakatime_project: wakatime_project.text


    GroupBox {
        Layout.fillWidth: true

        flat: true

        ColumnLayout {
            width: parent.width
            RowLayout {
                Text { width: indentWidth } // indent
                Label {
                    text: i18n("Enable: ")
                }
                CheckBox {
                    id: wakatime_enabled
                }
            }
            RowLayout {
                Text { width: indentWidth } // indent
                Label {
                    text: i18n("API Key: ")
                }
                TextField {
                    id: wakatime_api_key
                    Layout.fillWidth: true
                    enabled: cfg_wakatime_enabled
                    placeholderText: ""
                }
            }
            RowLayout {
                Text { width: indentWidth } // indent
                Label {
                    text: i18n("Project: ")
                }
                TextField {
                    id: wakatime_project
                    Layout.fillWidth: true
                    enabled: cfg_wakatime_enabled
                    placeholderText: ""
                }
            }
        }
    }

    Item {
        // tighten layout
        Layout.fillHeight: true
    }
}
