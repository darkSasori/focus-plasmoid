import org.kde.plasma.configuration 2.0
import org.kde.plasma.plasmoid 2.0

ConfigModel {
    ConfigCategory {
        name: i18n("General")
        icon: "chronometer"
        source: "configGeneral.qml"
    }

    ConfigCategory {
        name: i18n("WakaTime")
        icon: plasmoid.file("", "icons/wakatime.svg")
        source: "configWakatime.qml"
    }
}
